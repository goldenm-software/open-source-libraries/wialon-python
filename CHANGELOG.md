# Changelog
## Version 1.1.0
- General improvements

### Missing version changes (from v1.0.1 to v1.0.5)

## Version 1.0.1
- Fixed errors related to logout response
- Removed unused prints

## Version 1.0.0
- Initial version